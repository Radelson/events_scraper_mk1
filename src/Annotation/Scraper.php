<?php


namespace App\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("CLASS")
 */
class Scraper {

  /**
   * @Required
   *
   * @var string
   */
  public $name;

  /**
   * @Required
   *
   * @var string
   */
  public $id;

  /**
   * @Required
   * @var string
   */
  public $target_url;

  /**
   * @Required
   * @Enum({"EVENTS"})
   * @var string
   */
  public $target_type;
}