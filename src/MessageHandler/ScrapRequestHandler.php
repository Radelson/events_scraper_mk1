<?php


namespace App\MessageHandler;


use App\EventsLoaders\EntityEventLoader;
use App\EventsScrapers\EventScraperManager;
use App\Message\ScrapRequest;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ScrapRequestHandler implements MessageHandlerInterface {

  private $event_scraper_manager;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * @var \App\EventsLoaders\EntityEventLoader
   */
  private $eventLoader;

  public function __construct(EventScraperManager $eventScraperManager, LoggerInterface $logger, EntityEventLoader $eventLoader)
  {
    $this->event_scraper_manager = $eventScraperManager;
    $this->logger = $logger;
    $this->eventLoader = $eventLoader;
  }

  public function __invoke(ScrapRequest $scrap_request)
  {
    $scraper = $this->event_scraper_manager->create($scrap_request->getScraperName());
    $scrapped_events = $scraper->scrape($scrap_request->getAmountOfDays());
    $this->eventLoader->loadEvents($scrapped_events);
    $this->logger->info("Scraped events");
  }

}