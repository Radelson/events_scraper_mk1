<?php

namespace App\Message;

class ScrapRequest {

  private $scraperName;

  /**
   * @var int
   */
  private $days;

  public function __construct(string $scraperName, int $days) {
    $this->scraperName = $scraperName;
    $this->days = $days;
  }

  public function getScraperName(): string {
    return $this->scraperName;
  }
  
  public function getAmountOfDays(): string {
    return (int) $this->scraperName;
  }
}
