<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=EventRepository::class)
 */
class Event {

  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @Assert\NotBlank(message="No start date set")
   * @ORM\Column(type="datetime")
   */
  private $start_date;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   */
  private $end_date;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $source;

  /**
   * @ORM\Column(type="string", length=511)
   */
  private $detail_page_url;

  /**
   * @ORM\Column(type="string", length=511)
   */
  private $name;

  /**
   * @ORM\Column(name="description", type="text", length=65535, nullable=true)
   */
  private $description;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $image_path;

  /**
   * @return mixed
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param mixed $description
   */
  public function setDescription(?string $description): self {
    $this->description = $description;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName(?string $name): self {
    $this->name = $name;

    return $this;
  }

  public function getId(): ?int {
    return $this->id;
  }

  public function getStartDate(): ?\DateTimeInterface {
    return $this->start_date;
  }

  public function setStartDate(\DateTimeInterface $start_date): self {
    $this->start_date = $start_date;

    return $this;
  }

  public function getEndDate(): ?\DateTimeInterface {
    return $this->end_date;
  }

  public function setEndDate(?\DateTimeInterface $end_date): self {
    $this->end_date = $end_date;

    return $this;
  }

  public function getSource(): ?string {
    return $this->source;
  }

  public function setSource(string $source): self {
    $this->source = $source;

    return $this;
  }

  public function getDetailPageUrl(): ?string {
    return $this->detail_page_url;
  }

  public function setDetailPageUrl(string $detail_page_url): self {
    $this->detail_page_url = $detail_page_url;

    return $this;
  }

  public function getImagePath(): ?string {
    return $this->image_path;
  }

  public function setImagePath(?string $image_path): self {
    $this->image_path = $image_path;

    return $this;
  }
}
