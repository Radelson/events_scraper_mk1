<?php

namespace App\Command;

use App\EventsScrapers\EventScraperManager;
use App\Message\ScrapRequest;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

class ImportEventsCommand extends Command {

  protected static $defaultName = 'app:import-events';

  /**
   * @var MessageBusInterface
   */
  private $bus;

  /**
   * @var \App\EventsScrapers\EventScraperManager
   */
  private $event_scraper_manager;


  public function __construct(MessageBusInterface $bus, EventScraperManager $eventScraperManager)
  {
    $this->bus = $bus;
    $this->event_scraper_manager = $eventScraperManager;
    parent::__construct();
  }

  protected function configure() {
    $this
      ->setDescription('Import events')
      ->addOption('target-name',NULL, InputOption::VALUE_OPTIONAL, 'Only import from one source.')
      ->addOption('days',Null, InputOption::VALUE_OPTIONAL, 'Import specified amount of days for scrapers that supports it.');
  }

  protected function execute(InputInterface $input, OutputInterface $output): int {
    $io = new SymfonyStyle($input, $output);
    $arg1 = $input->getOption('target-name');
    $days = $input->getOption('days');

    if ($arg1) {
      $io->note(sprintf('You passed an argument: %s', $arg1));
    }

    if ($days) {
      $io->note(sprintf('You passed an argument: %s', $days));
    }

    foreach ($this->event_scraper_manager->getEventsScrapers() as $name => $scraper) {
      $this->bus->dispatch(new ScrapRequest($name, (int) $days));
    }

    $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

    return 0;
  }
}
