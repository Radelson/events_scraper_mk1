<?php

namespace App\EventsScrapers;


interface EventScraperInterface {

  /**
   * Does the work
   *
   * @param null $numbers
   *
   * @return NULL
   */
  public function scrape($numbers = NULL);

}