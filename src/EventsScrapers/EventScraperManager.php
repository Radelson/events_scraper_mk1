<?php


namespace App\EventsScrapers;

use Doctrine\Common\Annotations\AnnotationReader;
use Psr\Container\ContainerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

class EventScraperManager implements ServiceSubscriberInterface {

  /**
   * @var \App\EventsScrapers\EventScraperDiscovery
   */
  private $discovery;

  /**
   */
  private $eventScraperLocator;

  /**
   * EventScraperManager constructor.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $eventScraperLocator
   * @param \App\EventsScrapers\EventScraperDiscovery                 $eventScraperDiscovery
   */
  public function __construct(ContainerInterface $eventScraperLocator, EventScraperDiscovery $eventScraperDiscovery)
  {
    $this->discovery = $eventScraperDiscovery;
    $this->eventScraperLocator = $eventScraperLocator;
  }

  // @Todo : Probably this could be done using tagged services and tagged_locator ?
  public static function getSubscribedServices()
  {
    $finder = new Finder();
    $reader = new AnnotationReader();
    $scrapers = [];
    $finder->files()->in(__DIR__ . '/Scrapers');

    /** @var SplFileInfo $file */
    foreach ($finder as $file) {
      $class = __NAMESPACE__ . '\\Scrapers\\' . $file->getBasename('.php');
      $annotation = $reader->getClassAnnotation(new \ReflectionClass($class), '\App\Annotation\Scraper');
      if (!$annotation) {
        continue;
      }
      /** @var \App\Annotation\Scraper $annotation */
      $scrapers[$annotation->name] = $class;
    }
    return $scrapers;
  }

  /**
   * @param $name
   * @return array
   *
   * @throws \Exception
   */
  public function getEventScraper($name) {
    $scrapers = $this->discovery->getEventsScrapers();
    if (isset($scrapers[$name])) {
      return $scrapers[$name];
    }

    throw new \Exception('Scraper not found.');
  }

  /**
   * Creates a scraper
   *
   * @param $name
   *
   * @return object
   *
   * @throws \Exception
   */
  public function create($name) {
    if ($this->eventScraperLocator->has($name)) {
      return $this->eventScraperLocator->get($name);
    }
    throw new \Exception('scraper does not exists.');
  }

  public function getEventsScrapers() {
    $scrapers = $this->discovery->getEventsScrapers();
    if (isset($scrapers)) {
      return $scrapers;
    }

    throw new \Exception('Scrapers not found.');
  }

}