<?php


namespace App\EventsScrapers\Scrapers;

use App\Annotation\Scraper;
use App\Entity\Event;
use App\EventsScrapers\EventScraperBase;
use DateTime;
use IntlDateFormatter;
use Symfony\Component\Panther\Client;
use Symfony\Component\Panther\DomCrawler\Crawler;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @Scraper(
 *     id = "munda",
 *     name = "Mundaneum",
 *     target_url = "http://expositions.mundaneum.org/fr/agenda",
 *     target_type="EVENTS",
 * )
 */
class MundaneumScraper extends EventScraperBase {

  private $fmt;

  public function __construct(Client $client, Filesystem $fileSystem, $kernelProjectDir) {
    $this->fmt = new IntlDateFormatter(
      "fr-FR",
      IntlDateFormatter::FULL,
      IntlDateFormatter::FULL,
      'Etc/UTC',
      IntlDateFormatter::GREGORIAN,
      'd/m/Y'
    );
    return parent::__construct($client, $fileSystem, $kernelProjectDir);
  }

  /**
   * @inheritDoc
   */
  public function scrape($number_of_days = 10) {
    try {
      // Get all the links to events.
      $eventsLinks = $this->getEventsLinks($this->client->request('GET', $this->options->target_url), $number_of_days);
      //Call every event detail page and return the result.
      $events = [];
      foreach ($eventsLinks as $eventLink) {
        $events[] = $this->getEvent($this->client->request('GET', $eventLink));
      }
      return $events;
    } catch (\Exception $e) {
      // @todo : log smth and add an id/timing info in the file name + upload in particular dir
      $this->client->takeScreenshot($this->options->name . '.png');
      return [];
    }
  }

  /**
   * Should return an array of documents.
   *
   * @param $crawler
   * @param $number_of_days
   */
  private function getEventsLinks(Crawler $crawler, int $number_of_days) {
    $links = [];

    //For each event link on page, get the links.
    // @todo : Refactor this so we don't do the first iteration separately.
    $current_page_events = $crawler->filter(".agenda_element a.event_more")->links();
    foreach ($current_page_events as $event_link) {
      $links[$event_link->getUri()] = $event_link->getUri();
    }

    //Paginate over the the days to get events links on each days.
    $next_month_link = FALSE;
    $dayLinks = [];

    // While we have less than the defined days to scrape, minus the one we currently got.
    while (count($dayLinks) <= $number_of_days - 1) {
      // Get the links on the current page if the next month link is not yet scraped, otherwise scrape the next month.
      $currentPageCrawler = $next_month_link ? $this->client->click($next_month_link) : $crawler;
      // Get the links to the next days, slice it to get the exact amount desired.
      array_merge($dayLinks, $currentPageCrawler->filter(".future a")->slice(0, ($number_of_days - count($dayLinks) - 1))->links());
      $next_month_link = $crawler->filter("#agenda_nav_months_next")->link();
    }

    // Fetch every day gathered earlier.
    foreach ($dayLinks as $dayLink) {
      $this->client->click($dayLink);
      foreach ($this->client->waitFor('body')->filter(".agenda_element a.event_more")->links() as $event_link) {
        $links[$event_link->getUri()] = $event_link->getUri();
      }
    }

    return $links;
  }

  protected function extractName(\Symfony\Component\Panther\DomCrawler\Crawler $crawler) {
    if (($titleNode = $crawler->filter('h1.title')) && $titleNode->count() > 0) {
      return $titleNode->text();
    }
  }

  protected function extractStartDate(Crawler $crawler) {
    if (($date = $crawler->filter(".date-display-single")) && $date->count() > 0) {
      return new DateTime("@{$this->fmt->parse($date->text())}");
    }
    else {
      if (($startDate = $crawler->filter(".date-display-start")) && $startDate->count() > 0) {
        return new DateTime("@{$this->fmt->parse($startDate->text())}");
      }
    }
  }

  protected function extractEndDate(Crawler $crawler) {
    if (($endDate = $crawler->filter(".date-display-end")) && $endDate->count() > 0) {
      new DateTime("@{$this->fmt->parse($endDate->text())}");
    }
  }

  protected function extractDetailPageUrl(Crawler $crawler) {
    return $crawler->getUri();
  }

  protected function extractImagePath(Crawler $crawler) {
    if (($imgNode = $crawler->filter('.field-name-field-conference-visuel img')) && $imgNode->count() > 0) {
      return $this->downloadImg($imgNode->image()->getUri());
    }
    if (($imgNode = $crawler->filter('.field-name-field-ficheexpo-affiche img')) && $imgNode->count() > 0) {
      return $this->downloadImg($imgNode->image()->getUri());
    }
  }


}