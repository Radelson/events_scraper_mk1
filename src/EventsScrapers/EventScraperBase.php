<?php


namespace App\EventsScrapers;


use App\Entity\Event;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\BrowserKit\AbstractBrowser;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Panther\DomCrawler\Crawler;

abstract class EventScraperBase implements EventScraperInterface {

  const IMG_UPLOAD_DIR = '/public/files/events/img';

  /**
   * @var \Symfony\Component\Panther\Client
   */
  protected $client;

  /**
   * @var \Symfony\Component\Filesystem\Filesystem
   */
  protected $fileSystem;

  /**
   * @var \App\Annotation\Scraper
   */
  protected $options;

  private string $completeImgUploadDir;

  public function __construct(AbstractBrowser $client, Filesystem $fileSystem, $kernelProjectDir) {
    $this->client = $client;
    $this->fileSystem = $fileSystem;
    $this->completeImgUploadDir = $kernelProjectDir.self::IMG_UPLOAD_DIR;
    //@todo : This is not really nice. Probably this should be handled at the container level with a compiler pass ?
    //@todo : Maybe using annotations for this was a mistake but it feels nice being able to get metadata about the scraper without instanciating it.
    // Another nice thing is not mixing logic (the methods) and configuration (the annotations) for the scraper.
    $reader = new AnnotationReader();
    $this->options = $reader->getClassAnnotation(new \ReflectionClass(static::class), '\App\Annotation\Scraper');
  }

  /**
   * @todo : Probably strip the image style query string and try to get the unadultered img.
   * @todo : More checks and exceptions handling.
   * @param $uri
   *
   * @return string
   */
  protected function downloadImg($uri) {
    $basename = basename(parse_url($uri, PHP_URL_PATH));
    $uploadedFileName = $this->completeImgUploadDir.'/'.$basename;
    $this->fileSystem->dumpFile($uploadedFileName, file_get_contents($uri));
    return $uploadedFileName;
  }

  protected function getEvent(Crawler $crawler) {
    $event = new Event();
    $event_setters = array_filter(get_class_methods($event), function($method) {
      return 'set' === substr($method, 0, 3);
    });
    foreach ($event_setters as $key => $setter) {
      $extractor = substr_replace($setter, "extract", 0, 3);
      if (method_exists($this, $extractor) && method_exists($event, $setter)) {
        if ($extractedValue = $this->$extractor($crawler)) {
          $event->$setter($extractedValue);
        }
      }
    }
    return $event;
  }

  protected function extractName(Crawler $crawler) {
    return NULL;
  }
  protected function extractStartDate(Crawler $crawler) {
    return NULL;
  }
  protected function extractEndDate(Crawler $crawler) {
    return NULL;
  }
  protected function extractSource() {
    return $this->options->name;
  }
  protected function extractDetailPageUrl(Crawler $crawler) {
    return $crawler->getUri();
  }
  protected function extractImagePath(Crawler $crawler) {
    return NULL;
  }
}