<?php


namespace App\EventsScrapers;


use Doctrine\Common\Annotations\Reader;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class EventScraperDiscovery {

  /**
   * @var array
   */
  private $scrapers = [];

  /**
   * @var \Doctrine\Common\Annotations\Reader
   */
  private $annotation_reader;

  public function __construct(Reader $annotationReader) {
    $this->annotation_reader = $annotationReader;
  }

  /**
   * @return array
   */
  public function getEventsScrapers() {
    if (!$this->scrapers) {
      $this->discoverEventsScrapers();
    }
    return $this->scrapers;
  }

  /**
   * Discovers workers
   */
  private function discoverEventsScrapers() {
    $finder = new Finder();
    $finder->files()->in(__DIR__ . '/Scrapers');

    /** @var SplFileInfo $file */
    foreach ($finder as $file) {
      $class = __NAMESPACE__ . '\\Scrapers\\' . $file->getBasename('.php');
      $annotation = $this->annotation_reader->getClassAnnotation(new \ReflectionClass($class), '\App\Annotation\Scraper');
      if (!$annotation) {
        continue;
      }
      /** @var \App\Annotation\Scraper $annotation */
      $this->scrapers[$annotation->name] = [
        'class' => $class,
        'annotation' => $annotation,
      ];
    }
  }

  /**
   * @return array
   */
  public function getEventsScrapersClasses() {
    if (!$this->scrapers) {
      $this->discoverEventsScrapers();
    }
    return array_column($this->scrapers, 'class');
  }

}