<?php


namespace App\EventsLoaders;


use App\Entity\Event;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class EntityEventLoader {

  /**
   * @var \Doctrine\ORM\EntityManager
   */
  private $entityManager;

  /**
   * @var \Symfony\Component\Validator\Validator\ValidatorInterface
   */
  private $validator;

  /**
   * @var \App\Repository\EventRepository
   */
  private $eventRepository;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  public function __construct(LoggerInterface $logger, EntityManagerInterface $entityManager, ValidatorInterface $validator, EventRepository $eventRepository) {
    $this->logger = $logger;
    $this->entityManager = $entityManager;
    $this->validator = $validator;
    $this->eventRepository = $eventRepository;
  }

  /**
   * @param \App\Entity\Event[] $events
   */
  public function loadEvents($events) {
    foreach ($events as $event) {
      $this->loadEvent($event);
    }
  }

  private function loadEvent(Event $event) {
    if (($errors = $this->validator->validate($event)) && (count($errors) > 0)) {
      /** @var ConstraintViolationInterface $error */
      foreach ($errors as $error) {
        $this->logger->debug($error->getMessage());
      }
      return;
    }
    // Check if we already have an event with a matching name or source URL
    if (($name = $event->getName()) && ($events = $this->eventRepository->findBy(['name' => $name]))) {
      foreach ($events as $db_event) {
        if (hash('sha256', serialize($db_event)) !== hash('sha256', serialize($event))) {
          // Delete the old event.
          // @ Todo : Remove the files ?
          $this->entityManager->remove($db_event);
          $this->entityManager->flush();
        }
      }
    }
    if (($url = $event->getDetailPageUrl()) && ($events = $this->eventRepository->findBy(['detail_page_url' => $url]))) {
      foreach ($events as $db_event) {
        if (hash('sha256', serialize($db_event)) !== hash('sha256', serialize($event))) {
          // Delete the old event.
          // @ Todo : Remove the files ?
          $this->entityManager->remove($db_event);
          $this->entityManager->flush();
        }
      }
    }
    // Immediately commit the new event.
    $this->entityManager->persist($event);
    $this->entityManager->flush();
  }

}